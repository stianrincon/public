package com.charthop.sessionone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SessiononeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SessiononeApplication.class, args);
	}
}
