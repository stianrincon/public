package com.charthop.sessionone.controllers;

import com.charthop.sessionone.entity.Client;
import com.charthop.sessionone.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Transactional(rollbackFor = java.lang.Exception.class)
@RequestMapping("clients")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @PostMapping("")
    public Client save(@RequestBody Client client){
        return clientService.save(client);
    }
}
