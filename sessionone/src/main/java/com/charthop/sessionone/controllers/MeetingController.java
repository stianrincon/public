package com.charthop.sessionone.controllers;

import com.charthop.sessionone.entity.Client;
import com.charthop.sessionone.entity.Meeting;
import com.charthop.sessionone.service.MeetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Transactional(rollbackFor = java.lang.Exception.class)
@RequestMapping("meetings")
public class MeetingController {
    @Autowired
    private MeetingService meetingService;

    @PostMapping("")
    public Meeting save(@RequestBody Meeting meeting){
        return meetingService.save(meeting);
    }
}
