package com.charthop.sessionone.controllers;

import com.charthop.sessionone.entity.Meeting;
import com.charthop.sessionone.entity.Message;
import com.charthop.sessionone.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Transactional(rollbackFor = Exception.class)
@RequestMapping("meesages")
public class MessageController {
    @Autowired
    private MessageService messageService;

    @GetMapping("/meeting/{meetingId}")
    public List<Message> getMessageByMeetingId(@PathVariable Long meetingId){
        return messageService.findAllByMeetingId(meetingId);
    }
}
