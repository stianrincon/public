package com.charthop.sessionone.repository;

import com.charthop.sessionone.entity.Meeting;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MeetingRepo extends JpaRepository<Meeting, Long> {
}
