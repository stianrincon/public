package com.charthop.sessionone.repository;

import com.charthop.sessionone.entity.Client;
import com.charthop.sessionone.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageRepo extends JpaRepository<Message, Long> {

    List<Message> findAllByMeetingId(Long meetingId);
}
