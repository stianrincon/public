package com.charthop.sessionone.service;

import com.charthop.sessionone.entity.Client;
import com.charthop.sessionone.repository.ClientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = java.lang.Exception.class)
public class ClientService {

    @Autowired
    private ClientRepo clientRepo;

    public Client save(Client client){
        return clientRepo.save(client);
    }

}
