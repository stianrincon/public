package com.charthop.sessionone.service;

import com.charthop.sessionone.entity.Meeting;
import com.charthop.sessionone.entity.Message;
import com.charthop.sessionone.repository.MeetingRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor = java.lang.Exception.class)
public class MeetingService {
    @Autowired
    private MeetingRepo meetingRepo;

    public Meeting save(Meeting meeting){
        return meetingRepo.save(meeting);
    }
}
