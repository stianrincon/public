package com.charthop.sessionone.service;

import com.charthop.sessionone.entity.Message;
import com.charthop.sessionone.repository.MessageRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor = java.lang.Exception.class)
public class MessageService {

    @Autowired
    private MessageRepo messageRepo;

    public Message save(Message message){
        return messageRepo.save(message);
    }

    public List<Message> findAllByMeetingId(Long meetingId){
        return messageRepo.findAllByMeetingId(meetingId);
    }
}
