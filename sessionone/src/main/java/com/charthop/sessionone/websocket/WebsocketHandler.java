package com.charthop.sessionone.websocket;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.util.HashMap;
import java.util.List;

public class WebsocketHandler extends TextWebSocketHandler {

    HashMap<Integer, List<Integer>> clientMeetings = new HashMap<>();
    HashMap<Integer, List<Integer>> MeetingClients = new HashMap<>();

    private static int JOIN = 1;
    private static int CONNECT = 0;
    private static int DISCONNECT = 1;

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message)
            throws InterruptedException, IOException {

        String payload = message.getPayload();
        JSONObject jsonObject = new JSONObject(payload);
        session.sendMessage(new TextMessage("Hi " + jsonObject.get("user") + " how may we help you?"));
    }

    /**
     * clientId,
     * meetingId
     */
    public void join(WebSocketSession session, TextMessage message){
        clientMeetings.get(session.getId()).add(meetingId);
        MeetingClients.get(meetingId).add(session)
    }
}
